/**
 * Costa Rica National High Technology Center
 * Advanced Computing Laboratory
 * Costa Rica HPC School 2021 
 * Instructor: Esteban Meneses, PhD
 * MPI ping-pong program.
 */

#include <mpi.h>                                                                
#include <stdio.h>                                                              
#include <stdlib.h>                                                             
                                                                                
int main(int argc, char *argv[]) {                                               
                                                                                
  const int LIMIT = 1000;                                                         
                                                                                
  // Initialize the MPI environment                                             
  MPI_Init(&argc, &argv);                                                       
  // Find out rank, size                                                        
  int rank;                                                      
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);                                                                    
                                                                                                                                                                                                                                      
  int ping_pong_count = 0;                                                                                               
  while (ping_pong_count < LIMIT) {                                             
    if (rank == 0) {                                                          
                                                                                
      MPI_Send(&ping_pong_count, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);                          
      MPI_Recv(&ping_pong_count, 1, MPI_INT, 1, 0, MPI_COMM_WORLD,   
               MPI_STATUS_IGNORE);                                              
    }                                                                           
    else if(rank==1) {                                                        
      MPI_Recv(&ping_pong_count, 1, MPI_INT, 0, 0, MPI_COMM_WORLD,   
               MPI_STATUS_IGNORE);                                              
      // Increment the ping pong count before you send it                       
      ping_pong_count++;                                                        
      printf("Received and incremented ping_pong_count %d from 0 \n", ping_pong_count);                            
      MPI_Send(&ping_pong_count, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);  
    }                                                                           
  }                                                                             
  MPI_Finalize();
  return 0;                                                          
}




