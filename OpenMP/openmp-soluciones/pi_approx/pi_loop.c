#include <omp.h>
#include <stdio.h>


static long num_steps = 100000000;
double step;


int main(){
	int i;
	double pi = 0.0;
	
	double x;

	step = 1.0/(double) num_steps;

	double tdata = omp_get_wtime();

	#pragma omp parallel for reduction(+:pi) private(x)
	for(i=0; i<num_steps;i++){
		x = (i+0.5)*step;
		pi += 4.0/(1.0 + x*x);	
	}

	pi *= step;
	
	tdata = omp_get_wtime()-tdata;

	printf("pi = %.*f in %f secs\n", 12, pi, tdata);

}
